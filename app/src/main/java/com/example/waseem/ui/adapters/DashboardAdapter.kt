package com.example.waseem.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.waseem.R
import com.example.waseem.model.DashboardModel
import com.example.waseem.ui.GenericAdapterCallback

class DashboardAdapter(
    var context: Context,
    var list: List<DashboardModel.WalletCoin>,
    var genericAdapterCallback: GenericAdapterCallback
) :
    RecyclerView.Adapter<DashboardAdapter.ForgotViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ForgotViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.dashboard_child, parent, false)
        return ForgotViewHolder(v)
    }

    override fun onBindViewHolder(holder: ForgotViewHolder, position: Int) {
        holder.dash_coin_name.text = list.get(position).coinName
        holder.dash_curr_usd.text = list.get(position).coinBalInUsd
        holder.dash_curr.text = list.get(position).coinBalInCurr
        Glide.with(context)
            .load(list.get(position).imgUrl)
            .into(holder.dash_img)

        holder.dash_parent.setOnClickListener {
            genericAdapterCallback.getClickedObject(list.get(position), position)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int = list.size

    class ForgotViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val dash_img = itemView.findViewById<ImageView>(R.id.dash_img)
        val dash_coin_name = itemView.findViewById<TextView>(R.id.dash_coin_name)
        val dash_curr_usd = itemView.findViewById<TextView>(R.id.dash_curr_usd)
        val dash_curr = itemView.findViewById<TextView>(R.id.dash_curr)
        val dash_parent = itemView.findViewById<ConstraintLayout>(R.id.dash_parent)
    }
}