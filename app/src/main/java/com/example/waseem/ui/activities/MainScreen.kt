package com.example.waseem.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.waseem.R
import com.example.waseem.utils.navigate
import kotlinx.android.synthetic.main.activity_main_screen.*

class MainScreen : AppCompatActivity() , View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_screen)
        initListeners()
    }

    private fun initListeners() {
        fast_wallet.setOnClickListener(this)
        advanced_wallet.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.fast_wallet -> {
                //do nothing for the time being
            }

            R.id.advanced_wallet -> {
                navigate<StringCodes>()
            }
        }
    }
}
