package com.example.waseem.ui.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.waseem.R
import com.example.waseem.model.CurrenciesList
import com.example.waseem.model.ERCurrency
import com.example.waseem.ui.adapters.CurrencyAdapter
import com.example.waseem.ui.viewmodel.CurrenciesListViewModel
import com.example.waseem.utils.*
import kotlinx.android.synthetic.main.activity_currencies_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class CurrenciesListActivity : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + compositeJob

    val compositeJob = Job()
    lateinit var viewmodel: CurrenciesListViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currencies_list)
        appToolBar(this@CurrenciesListActivity, toolbar6)
        initData()
        callERCurrenciesApi()
    }

    var erCurrencies: ERCurrency? = null

    private fun callERCurrenciesApi() {
        if (checkNetworkConnectivity(this@CurrenciesListActivity)) {
            showProgressDialog(this@CurrenciesListActivity)
            launch {
                try {
                    erCurrencies = viewmodel.getERCurrencies()
                    if (erCurrencies != null) {
                        callCurrenciesListApi(erCurrencies!!)
                    }
                    dismissDialog()

                } catch (exception: Exception) {
                    exception.printStackTrace()
                    dismissDialog()
                }
            }
        } else {
            showToast(CHECK_INTERNET)
        }
    }

    private fun initData() {
        viewmodel = ViewModelProviders.of(this).get(CurrenciesListViewModel::class.java)
    }

    var currencies: CurrenciesList? = null

    private fun callCurrenciesListApi(erCurrencies: ERCurrency) {
        if (checkNetworkConnectivity(this@CurrenciesListActivity)) {
            showProgressDialog(this@CurrenciesListActivity)
            launch {
                try {
                    currencies = viewmodel.getCurrencyListApi()
                    if(currencies != null) {
                        createData(erCurrencies, currencies)
                    } else {
                        showToast("Something went wrong")
                    }
                    dismissDialog()

                } catch (exception: Exception) {
                    exception.printStackTrace()
                    dismissDialog()
                }
            }
        } else {
            showToast(CHECK_INTERNET)
        }
    }

    private fun createData(erCurrencies: ERCurrency, currencies: CurrenciesList?) {
        for (data in erCurrencies.currenies) {
            if (currencies?.rates?.get(data.coin_key.toLowerCase()) != null) {
                data.coin_value = currencies.rates.get(data.coin_key.toLowerCase())!!

            } else if (currencies?.rates?.get(data.coin_key.toUpperCase()) != null) {
                data.coin_value = currencies.rates.get(data.coin_key.toUpperCase())!!
            } else {
                data.coin_value = ""
            }
        }

        val adapter = CurrencyAdapter(this@CurrenciesListActivity, erCurrencies.currenies)
        coin_recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
