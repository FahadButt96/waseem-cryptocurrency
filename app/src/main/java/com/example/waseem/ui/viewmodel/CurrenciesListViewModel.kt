package com.example.waseem.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.waseem.model.CurrenciesList
import com.example.waseem.model.ERCurrency
import com.example.waseem.ui.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CurrenciesListViewModel : ViewModel() {

    suspend fun getCurrencyListApi(): CurrenciesList? {
        try {
            val obj = withContext(Dispatchers.IO) {
                ApiInterface.coroutineCreate("http://api.coinlayer.com/api/").currenciesList().await()
            }

            return obj.body()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    suspend fun getERCurrencies(): ERCurrency? {
        try {
            val obj = withContext(Dispatchers.IO) {
                ApiInterface.coroutineCreate().ercCurrencies().await()
            }

            return obj.body()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}