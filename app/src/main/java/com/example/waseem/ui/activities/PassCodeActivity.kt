package com.example.waseem.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.waseem.R
import com.example.waseem.utils.navigate
import kotlinx.android.synthetic.main.activity_pass_code.*

class PassCodeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ll_forget -> {
                navigate<ForgetPasscodeActivity>()
            }
            R.id.check_in -> {
                navigate<MainActivity>()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pass_code)

        init()
    }

    private fun init() {
        check_in.setOnClickListener(this)
        ll_forget.setOnClickListener(this)
    }
}
