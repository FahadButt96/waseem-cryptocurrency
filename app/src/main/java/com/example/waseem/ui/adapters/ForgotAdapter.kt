package com.example.waseem.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.waseem.R

class ForgotAdapter(
    var context: Context
) :
    RecyclerView.Adapter<ForgotAdapter.ForgotViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ForgotViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.forgot_layout, parent, false)
        return ForgotViewHolder(v)
    }

    override fun onBindViewHolder(holder: ForgotViewHolder, position: Int) {
        holder.textView.text = "" + (position + 1)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int = 12

    class ForgotViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView = itemView.findViewById<TextView>(R.id.textView)
    }
}