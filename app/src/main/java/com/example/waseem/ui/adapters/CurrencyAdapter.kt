package com.example.waseem.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.waseem.R
import com.example.waseem.model.ERCurrency

class CurrencyAdapter(
    var context: Context ,
    var list: List<ERCurrency.Curreny>
) :
    RecyclerView.Adapter<CurrencyAdapter.ForgotViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ForgotViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.currency_child, parent, false)
        return ForgotViewHolder(v)
    }

    override fun onBindViewHolder(holder: ForgotViewHolder, position: Int) {
        holder.coin_name.text = list.get(position).coin_name
        holder.coin_value.text = list.get(position).coin_value
        Glide.with(context)
            .load(list.get(position).img_url)
            .into(holder.coin_image)
        holder.parent.setOnClickListener {
            if(holder.checkBox.isChecked){
                holder.checkBox.isChecked = false
                list.get(position).isChecked = false
            } else {
                holder.checkBox.isChecked = true
                list.get(position).isChecked = true
            }
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int = list.size

    class ForgotViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val coin_image = itemView.findViewById<ImageView>(R.id.coin_image)
        val coin_name = itemView.findViewById<TextView>(R.id.coin_name)
        val coin_value = itemView.findViewById<TextView>(R.id.coin_value)
        val parent = itemView.findViewById<ConstraintLayout>(R.id.parent)
        val checkBox = itemView.findViewById<CheckBox>(R.id.checkBox)
    }
}