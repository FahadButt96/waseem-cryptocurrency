package com.example.waseem.ui.activities

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.waseem.R
import com.example.waseem.model.Words
import com.example.waseem.ui.viewmodel.StringCodesViewModel
import com.example.waseem.utils.*
import kotlinx.android.synthetic.main.activity_string_codes.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class StringCodes : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + compositeJob

    val compositeJob = Job()

    lateinit var viewmodel: StringCodesViewModel
    lateinit var sharedPrefUtil: SharedPrefUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_string_codes)
        appToolBar(this , toolbar3)
        initData()
        initListeners()
        callWordsApi()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
    var wordsData: Words? = null
    private fun callWordsApi() {
        if (checkNetworkConnectivity(this@StringCodes)) {
            showProgressDialog(this@StringCodes)
            launch {
                try {
                    wordsData = viewmodel.getWordsApi()
                    dismissDialog()
                    if (wordsData != null) {
                        words.setText("")
                        var str = ""
                        for (data in wordsData?.words!!) {
                            words.setText(words.text.toString() + " " + data)
                            str = str + data + " "
                        }
                        sharedPrefUtil.saveSharedPrefValue(this@StringCodes , "words" , str.trim())
                    }
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    dismissDialog()
                }
            }
        } else {
            showToast(CHECK_INTERNET)
        }
    }

    private fun initData() {
        viewmodel = ViewModelProviders.of(this).get(StringCodesViewModel::class.java)
        sharedPrefUtil = SharedPrefUtil()
    }

    private fun initListeners() {

        stored_checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                next_btn.visibility = View.VISIBLE
            } else {
                next_btn.visibility = View.GONE
            }
        }
        next_btn.setOnClickListener {
            navigate<AddWords>()
        }
    }
}
