package com.example.waseem.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.waseem.model.Words
import com.example.waseem.ui.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StringCodesViewModel : ViewModel() {

    suspend fun getWordsApi(): Words? {
        try {
            val obj = withContext(Dispatchers.IO) {
                ApiInterface.coroutineCreate().wordsApi().await()
            }

            return obj.body()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}