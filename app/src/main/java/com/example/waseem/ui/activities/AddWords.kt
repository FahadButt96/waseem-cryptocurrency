package com.example.waseem.ui.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.waseem.R
import com.example.waseem.utils.SharedPrefUtil
import com.example.waseem.utils.appToolBar
import com.example.waseem.utils.navigate
import com.example.waseem.utils.showToast
import kotlinx.android.synthetic.main.activity_add_words.*

class AddWords : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_words)
        appToolBar(this@AddWords , toolbar4)
        initListeners()
    }

    private fun initListeners() {
        next_button.setOnClickListener {
            compareString()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun compareString() {
        var str =
            "${word_1.text} ${word_2.text} ${word_3.text} ${word_4.text} ${word_5.text} ${word_6.text} ${word_7.text} ${word_8.text} ${word_9.text} " +
                    "${word_10.text} ${word_11.text} ${word_12.text}"
        var shared_prf_str = SharedPrefUtil().getSharedPrefValue(this@AddWords, "words")
        if (str.equals(shared_prf_str, true)) {
            navigate<PasswordAuthentication>()
        } else {
            showToast("Recovery Phrase not matched!")
        }
    }
}
