package com.example.waseem.ui

import com.example.waseem.model.*
import com.example.waseem.utils.BASE_URLS
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface ApiInterface {

    @GET("mwords")
    fun wordsApi(): Deferred<Response<Words>>

    @GET("ercCurr")
    fun ercCurrencies(): Deferred<Response<ERCurrency>>


    @GET("live?access_key=86a058c728cfc1806e1af0d9c97f628c")
    fun currenciesList(): Deferred<Response<CurrenciesList>>

    @GET("create_wallet")
    fun create_wallet(@Query("mwords") create_wallet: String, @Query("password") password: String): Deferred<Response<CreateWallet>>


    @GET("walletProfile")
    fun getWalletProfile(@Query("wallet_add") wallet_add: String): Deferred<Response<DashboardModel>>


    companion object {
        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URLS)
                .build()

            return retrofit.create(ApiInterface::class.java)

        }

        fun coroutineCreate(url: String = BASE_URLS): ApiInterface {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(makeOkHttpClient())
                .baseUrl(url)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }

        private fun makeOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .connectTimeout(50, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .build()
        }
    }
}