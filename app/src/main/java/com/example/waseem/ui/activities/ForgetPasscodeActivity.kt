package com.example.waseem.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.example.waseem.R
import com.example.waseem.ui.adapters.ForgotAdapter
import com.example.waseem.utils.navigate
import kotlinx.android.synthetic.main.activity_forget_passcode.*

class ForgetPasscodeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.check_in -> {
                navigate<MainActivity>()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_passcode)

        init_view()
    }

    fun init_view() {

        check_in.setOnClickListener(this)

        forget_rv.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        forget_rv.adapter = ForgotAdapter(this)
    }
}
