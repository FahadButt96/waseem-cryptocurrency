package com.example.waseem.ui.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.waseem.R
import com.example.waseem.model.CreateWallet
import com.example.waseem.ui.viewmodel.PasswordAuthenticationViewModel
import com.example.waseem.utils.*
import kotlinx.android.synthetic.main.activity_password_authentication.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class PasswordAuthentication : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + compositeJob

    val compositeJob = Job()

    lateinit var viewmodel: PasswordAuthenticationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_authentication)
        appToolBar(this@PasswordAuthentication , toolbar5)
        initData()
        initView()
    }

    private fun initData() {
        viewmodel = ViewModelProviders.of(this).get(PasswordAuthenticationViewModel::class.java)

    }

    private fun initView() {
        done_button.setOnClickListener {
            if (password.text.toString().equals(confirm_password.text.toString(), true)) {
                callCreateWalletApi()
            } else {
                showToast("Password does not match.")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    var create_wallet: CreateWallet? = null
    private fun callCreateWalletApi() {
        if (checkNetworkConnectivity(this@PasswordAuthentication)) {
            showProgressDialog(this@PasswordAuthentication)
            launch {
                try {
                    create_wallet = viewmodel.createWallet(
                        SharedPrefUtil().getSharedPrefValue(
                            this@PasswordAuthentication,
                            "words"
                        )!!, confirm_password.text.toString()
                    )
                    if(create_wallet != null) {
                        if(create_wallet?.resp!!) {
                            SharedPrefUtil().saveSharedPrefValue(this@PasswordAuthentication , "wallet_address" , create_wallet?.data?.wallet_address!!)
                        }
                        navigate<MainActivity>(clearPreviousActivity = true)
                    }
                    dismissDialog()
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    dismissDialog()
                }
            }
        } else {
            showToast(CHECK_INTERNET)
        }
    }

}
