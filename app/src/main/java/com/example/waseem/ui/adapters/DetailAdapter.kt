package com.example.waseem.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.waseem.R

class DetailAdapter(
    var context: Context
) :
    RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): DetailViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.detail_items, parent, false)
        return DetailViewHolder(v)
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int = 12

    class DetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}