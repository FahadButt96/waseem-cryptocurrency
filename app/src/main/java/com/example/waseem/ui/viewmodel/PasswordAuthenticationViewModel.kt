package com.example.waseem.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.waseem.model.CreateWallet
import com.example.waseem.model.Words
import com.example.waseem.ui.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PasswordAuthenticationViewModel : ViewModel() {

    suspend fun createWallet(words : String , password : String): CreateWallet? {
        try {
            val obj = withContext(Dispatchers.IO) {
                ApiInterface.coroutineCreate().create_wallet(words, password).await()
            }

            return obj.body()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}