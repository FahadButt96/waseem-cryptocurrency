package com.example.waseem.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.waseem.model.DashboardModel
import com.example.waseem.ui.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DashboardViewModel : ViewModel() {

    suspend fun getDashboardData(wallet_add: String): DashboardModel? {
        try {
            val obj = withContext(Dispatchers.IO) {
                ApiInterface.coroutineCreate().getWalletProfile(wallet_add).await()
            }

            return obj.body()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

}