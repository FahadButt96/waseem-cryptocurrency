package com.example.waseem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.waseem.model.DashboardModel
import com.example.waseem.ui.GenericAdapterCallback
import com.example.waseem.ui.adapters.DashboardAdapter
import com.example.waseem.ui.viewmodel.DashboardViewModel
import com.example.waseem.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class Dashboard : Fragment(), GenericAdapterCallback,
    CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + compositeJob

    val compositeJob = Job()
    lateinit var navGraph: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navGraph = Navigation.findNavController(view)
        activity!!.toolbar_title.setText("Dashboard")
        callDashboardApi()
    }

    var dashboard: DashboardModel? = null
    private fun callDashboardApi() {
        if (checkNetworkConnectivity(activity!!)) {
            showProgressDialog(activity!!)
            launch {
                try {
                    dashboard = DashboardViewModel().getDashboardData(
                        SharedPrefUtil().getSharedPrefValue(
                            activity!!,
                            "wallet_address"
                        )!!
                    )
                    if (dashboard != null) {
                        if (dashboard?.walletCoins != null) {
                            setRecyclerView(dashboard?.walletCoins!!)
                        } else {
                            activity!!.showToast("something went wring")
                        }
                    }
                    dismissDialog()

                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        } else {
            activity!!.showToast(CHECK_INTERNET)
        }
    }

    private fun setRecyclerView(walletCoins: List<DashboardModel.WalletCoin>) {
        val adapter = DashboardAdapter(activity!!, walletCoins, this@Dashboard)
        dashboard_recyclerview.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun <T> getClickedObject(clickedObj: T, position: T, callingID: String) {
        clickedObj as DashboardModel.WalletCoin

        navGraph.navigate(R.id.detailPage)

    }
}
