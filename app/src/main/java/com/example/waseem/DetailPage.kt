package com.example.waseem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.waseem.ui.adapters.FragmentStatePageAdapter
import kotlinx.android.synthetic.main.fragment_detail_page.*

class DetailPage : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_page, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewPager()
        detail_viewpager.setCurrentItem(1, true)
    }


    lateinit var viewPagerAdapter: FragmentStatePageAdapter
    private fun setUpViewPager() {
        viewPagerAdapter = FragmentStatePageAdapter(childFragmentManager)
        viewPagerAdapter.addFragment(SendToken(), "Send")
        viewPagerAdapter.addFragment(SendToken(), "Balance")
        viewPagerAdapter.addFragment(SendToken(), "Receive")

        detail_viewpager.offscreenPageLimit = 0
        detail_viewpager.adapter = viewPagerAdapter
        detail_tabLayout.setupWithViewPager(detail_viewpager)
    }
}
