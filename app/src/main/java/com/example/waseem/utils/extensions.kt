package com.example.waseem.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.waseem.R
import com.example.waseem.model.IntentParams

inline fun <reified T : Activity> Activity.navigate(
    params: List<IntentParams> = emptyList(),
    clearPreviousActivity: Boolean = false
) {

    val intent = Intent(this, T::class.java)
    for (parameter in params) {
        intent.putExtra(parameter.key, parameter.value)
    }
    if (clearPreviousActivity) {
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }
    startActivity(intent)
    if (clearPreviousActivity) {
        finish()
    }
}

var mProgressDialog: ProgressDialog? = null

fun checkNetworkConnectivity(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = cm.activeNetworkInfo
    return netInfo != null && netInfo.isConnectedOrConnecting && (netInfo.type == ConnectivityManager.TYPE_MOBILE || netInfo.type == ConnectivityManager.TYPE_WIFI)
}

fun Activity.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}


var dialog: AlertDialog? = null
fun showProgressDialog(context: Context) {
    if (dialog == null) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false) // if you want user to wait for some process to finish,

        builder.setView(R.layout.progress_dialog)
        dialog = builder.create()
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.setCancelable(false)
        dialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    if (context != null) {
        dialog!!.show()
    }
}

fun dismissDialog() {
    if (dialog != null) {
        dialog!!.dismiss()
        dialog = null
    }
}


// Application Toolbar
fun appToolBar(activity: Context, toolbar: Toolbar?, isSetDisplayHomeAsUpEnabled: Boolean = true
               , isSetDisplayShowHomeEnabled: Boolean = true, isSetDisplayShowTitleEnabled: Boolean = false): Unit {

    (activity as AppCompatActivity).setSupportActionBar(toolbar!!)
    activity.supportActionBar!!.setDisplayHomeAsUpEnabled(isSetDisplayHomeAsUpEnabled)
    activity.supportActionBar!!.setDisplayShowHomeEnabled(isSetDisplayShowHomeEnabled)
    activity.supportActionBar!!.setDisplayShowTitleEnabled(isSetDisplayShowTitleEnabled)
}


