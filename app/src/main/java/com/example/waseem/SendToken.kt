package com.example.waseem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.waseem.utils.showToast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_send_token.*

class SendToken : Fragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_send_token, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity!!.toolbar_title.setText("Send Token")
        initListeners()
    }

    private fun initListeners() {
        send_btn.setOnClickListener(this)
        send_slow.setOnClickListener(this)
        send_fast.setOnClickListener(this)
        send_fastest.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.send_btn -> {
                activity!!.showToast("Send Btn")
            }
            R.id.send_fast -> {
                activity!!.showToast("send_fast")
            }

            R.id.send_slow -> {
                activity!!.showToast("send_slow")
            }
            R.id.send_fastest -> {
                activity!!.showToast("send_fastest")
            }

        }
    }
}
