package com.example.waseem.model

data class ERCurrency(
    val currenies: ArrayList<Curreny>
) {
    data class Curreny(
        val coin_key: String,
        val coin_name: String,
        val img_url: String,
        var coin_value : String,
        var isChecked : Boolean = false
    )
}