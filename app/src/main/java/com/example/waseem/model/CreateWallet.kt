package com.example.waseem.model

data class CreateWallet(
    val `data`: Data,
    val resp: Boolean
) {
    data class Data(
        val user_id: String,
        val wallet_address: String
    )
}