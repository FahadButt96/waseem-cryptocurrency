package com.example.waseem.model


import com.google.gson.annotations.SerializedName

data class DashboardModel(
    @SerializedName("wallet_balance")
    val walletBalance: String,
    @SerializedName("wallet_coins")
    val walletCoins: List<WalletCoin>,
    @SerializedName("wallet_curr")
    val walletCurr: String
) {
    data class WalletCoin(
        @SerializedName("coin_bal_in_curr")
        val coinBalInCurr: String,
        @SerializedName("coin_bal_in_usd")
        val coinBalInUsd: String,
        @SerializedName("coin_key")
        val coinKey: String,
        @SerializedName("coin_name")
        val coinName: String,
        @SerializedName("img_url")
        val imgUrl: String
    )
}