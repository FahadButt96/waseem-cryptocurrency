package com.example.waseem.model

import com.google.gson.annotations.SerializedName

data class CurrenciesList(
    @SerializedName("privacy")
    val privacy: String,
    @SerializedName("rates")
    val rates: HashMap<String , String>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("target")
    val target: String,
    @SerializedName("terms")
    val terms: String,
    @SerializedName("timestamp")
    val timestamp: Int
)
